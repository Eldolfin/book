# Introducing Redox OS

Redox OS is microkernel-based operating system, with a large number of supporting programs and components, to create a full-featured user and application environment. In this chapter, we will discuss the goals, philosophy and scope of Redox.