# Developing for Redox

Redox does not yet have a complete set of development tools that run natively. Currently, you must do your development on Linux, then include or copy your application to your Redox filesystem. This chapter outlines some of the things you can do as a developer.