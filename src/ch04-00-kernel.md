The kernel of Redox
===================

The Redox kernel largely derives from the concept of microkernels, with particular inspiration from [MINIX](https://en.wikipedia.org/wiki/MINIX
).

This chapter will discuss the design of the Redox kernel.
