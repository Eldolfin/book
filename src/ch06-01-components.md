Components of Redox
===================

Redox is made up of several discrete components. 

* ion - shell
* TFS/RedoxFS - filesystem
* kernel 
* drivers
* orbital - display server/window manager/desktop environment

## Orbital subcomponents
* orbterm - terminal
* orbdata - images, fonts, etc.
* orbaudio - audio
* orbutils - bunch of applications
* orblogin - login prompt
* orbtk - cross-platform Rust GUI toolkit, similar to GTK
* orbfont - font rendering library
* orbclient - display client
* orbimage - image rendering library 

## Core Applications
* Sodium - text editor
* orbutils
  * background
  * browser
  * calculator
  * character map
  * editor
  * file manager
  * launcher
  * viewer
