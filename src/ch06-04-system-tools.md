System Tools
============

Coreutils
---------

Coreutils is a collection of basic command line utilities included with Redox (or with Linux, BSD, etc.). This includes programs like `ls`, `cp`, `cat` and various other tools necessary for basic command line interaction.

Redox's coreutils aim to be more minimal than, for instance, the GNU coreutils included with most Linux systems.

Binutils
--------

Binutils contains utilities for manipulating binary files. Currently Redox's binutils includes `strings`, `disasm`, `hex`, and `hexdump`

Extrautils
----------

Some additional command line tools are included in extrautils, such as `less`, `grep`, and `dmesg`.
